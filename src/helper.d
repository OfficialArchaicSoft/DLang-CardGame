﻿module helper;

public import std.conv : text, to;
public import std.stdio : readln, writeln;

void clear()
{
	import core.stdc.stdlib;
	system("@cls||clear");
}

@property string readLine()
{
	import std.string : chomp;
	return readln.chomp();
}

string commandInput() {
	import std.string : toLower;
	writeln("Command > ");
	return toLower(trim(readLine));
}

bool contains(string input, string seek)
{
	import std.algorithm.searching : find;
	return find(input, seek).length > 0;
}

bool multiWorded(string input)
{
	return input.contains(" ");
}

size_t toNum(string input)
{
	import std.ascii : isDigit;
	import std.conv : to;

	string value = "0";
	foreach (c; trim(input))
	{
		if (!isDigit(c)) break;
		value ~= c;
	}
	
	return to!size_t(value);
}

string trim(string input)
{
	import std.string : strip;
	return trimMid(strip(input));
}

string trimMid(string input)
{
	import std.array : replace;

	while (input.contains("  "))
		input = input.replace("  ", " ");

	return input;
}

string[] splitStr(string input)
{
	import std.array : split;
	import std.uni : isWhite;
	
	return input.split!isWhite();
}