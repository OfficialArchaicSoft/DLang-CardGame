﻿module classic.test;
import classic.base;

class TestGame : ClassicBase
{
	override @property size_t minPlayers() { return 2; }
	override @property size_t maxPlayers() { return 4; }

	bool exit;

	override void playGame() {
		/* Intro */
		{
			writeln("Let's play - Test!\nPress return to begin...");
			readln();
		}
		
		/* Actual game */
		while (!exit) {
			/* Player turn handled here */
			for (size_t i = 0; i < player.length; i++) {
				refreshPlayerScreen(i);
				
				getInput(i, commandInput);
				if (player[i].count == 0) {
					writeln("Player ", i+1 , " is out of cards!");
					exit = true;
					break;
				}
			}
		}
		
		/* Exit game */
		{
			writeln("Exiting game... Press return to continue.");
			readln();
		}
	}
	
	override void getInput(size_t index, string input) {
		if (input.length > 0) {	
			if (multiWorded(input)) {
				string[] arg = splitStr(input);
				
				if (arg[0] == "/help") {
					if (arg[1] == "draw") writeln("draw (optional)[amount]: Draws the amount of cards. Defaults to 1 if not specified");
					else if (arg[1] == "play") writeln("play [slot]: Plays the card in the slot. Use hand to see available cards.");
					else if (arg[1] == "clear") writeln("clear : Clears the screen of all text.");
					else if (arg[1] == "end") writeln("end : Ends the current players turn.");
					else if (arg[1] == "playercount") writeln("playercount : Lists the amount of players in the game.");
					else if (arg[1] == "discard") writeln("discard (optional)[-l]: Lists information about the discard pile.");
					else if (arg[1] == "exit") writeln("exit : Ends the game and closes the application.");
				}
				else if (arg[0] == "draw") {
					size_t count = toNum(arg[1]);
					drawCard(index, count);
				}
				else if (arg[0] == "play") {
					size_t slot = toNum(arg[1]);
					if (slot >= player[index].count) 
						writeln("Error: Invalid slot. Please enter [0-", player[index].count, "]. Use hand to see available cards."); 
					else discard.insert(player[index].draw(1, slot));
				}
				else if (arg[0] == "discard") {
					if (arg[1] == "-l") {
						if (discard.count == 0) writeln("Discard pile is empty.");
						else {
							foreach(c; discard.card) writeln(c.toString());
						}
					}
					else writeln("Error: Invalid args. Optional arg(s) are [-l].");
				}
				else writeln("Error: Invalid arguments. Check /help for available commands.");
			}
			else {
				if (input == "/help") {
					writeln();
					writeln("[Available commands]");
					writeln();
					writeln("draw (optional)[amount]: Draws the amount of cards. Defaults to 1 if not specified");
					writeln("play [slot]: Plays the card in the slot. Use hand to see available cards.");
					writeln("clear : Clears the screen of all text.");
					writeln("end : Ends the current players turn.");
					writeln("playercount : Lists the amount of players in the game.");
					writeln("discard (optional)[-l]: Lists information about the discard pile.");
					writeln("exit : Ends the game and closes the application.");
					writeln();
				}
				else if (input == "draw") drawCard(index);
				else if (input == "play") writeln("Error: Not enough arguments. Check /help play for available commands.");
				else if (input == "clear") refreshPlayerScreen(index);
				else if (input == "end") { return; }
				else if (input == "playercount") writeln("Player Amount: ", player.length);
				else if (input == "discard") {
					if (discard.count == 0) writeln("Discard pile is empty.");
					else writeln(discard.view(ClassicBase.Stack.bottom));
				}
				else if (input == "exit") { exit  = true; return; }		
				else writeln("Error: Invalid arguments. Check /help for available commands.");
			}
		}
		
		/* If out of cards then exit to win check - Else wait for more input */
		{
			if (player[index].count == 0) return;
			getInput(index, commandInput);
		}
	}
}